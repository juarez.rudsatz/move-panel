# Move Panel

Gnome shell extension, which moves the panel to a secondary screen, without changing the primary monitor. Fork of https://github.com/Noobsai/fullscreen-avoider. Instead of moving the panel when entering fullscreen, this extension moves the panel on startup, which prevents wonky window sizes and makes the setup feel more stable.

The main application for this is gaming with multiple monitors. Many games launch on whichever screen is set as primary monitor, this extension ensures that the panel will be on a different monitor, so it can still be seen and interacted with. Unfortunately, I could not figure out how to set the panel to a specific monitor, so currently it just picks the first one it can find, that is not the primary monitor. This means that the extension works best with two monitors.

Extension has only been tested on wayland. Currently it does not revert to the original panel position when disabling the extension, this can be fixed by toggling the gnome display settings accordingly.

## Installation

### Gnome Extensions

The extension can be found here: https://extensions.gnome.org/extension/4890/move-panel/

### Git

```bash
git clone https://gitlab.com/lzbz/move-panel.git
cd move-panel && cp -r move-panel@lzbz.gitlab.com ~/.local/share/gnome-shell/extensions 
```